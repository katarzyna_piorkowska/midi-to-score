package com.midi.miditoscore;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_score);

        Button button_return = (Button) findViewById(R.id.button_go_back);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_return.setTypeface(typeface);

        button_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        final EditText music_name =  (EditText) findViewById(R.id.text_edit);
        final EventReader read = new EventReader(this, 'c');


        Button button_create_OK = (Button) findViewById(R.id.button_create_OK);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_create_OK.setTypeface(typeface);

        button_create_OK.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                read.get_note(name, " ", 0, 0);

            }
        });

    }
}
