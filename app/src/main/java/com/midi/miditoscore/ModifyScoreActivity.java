package com.midi.miditoscore;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ModifyScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_score);

        Button button_return = (Button) findViewById(R.id.button_go_back_mod);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_return.setTypeface(typeface);

        button_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });



        Button button_change_tempo = (Button) findViewById(R.id.button_change_tempo);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_change_tempo.setTypeface(typeface);

        button_change_tempo.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                Intent i = new Intent(getBaseContext(), ChangeTempoActivity.class);
                startActivity(i);
            }
        });

        Button button_silence = (Button) findViewById(R.id.button_silence_track);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_silence.setTypeface(typeface);

        button_silence.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                Intent i = new Intent(getBaseContext(), SilenceTrackActivity.class);
                startActivity(i);
            }
        });

    }
}
