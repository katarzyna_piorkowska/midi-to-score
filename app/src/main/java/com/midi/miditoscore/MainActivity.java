package com.midi.miditoscore;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;

import com.leff.midi.MidiFile;
import com.leff.midi.MidiTrack;
import com.leff.midi.event.meta.Tempo;
import com.leff.midi.event.meta.TimeSignature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
{
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView title = (TextView) findViewById(R.id.title);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/magnolia.ttf");
        title.setTypeface(typeface);

        Button button_create = (Button) findViewById(R.id.button_create_score);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_create.setTypeface(typeface);

        button_create.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                Intent i = new Intent(getBaseContext(), CreateScoreActivity.class);
                startActivity(i);
            }
        });

        Button button_modify = (Button) findViewById(R.id.button_mod_score);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_modify.setTypeface(typeface);

        button_modify.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                Intent i = new Intent(getBaseContext(), ModifyScoreActivity.class);
                startActivity(i);
            }
        });

    }
}