package com.midi.miditoscore;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SilenceTrackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_silence_track);

        final EditText music_name =  (EditText) findViewById(R.id.text_edit_mod);
        final EditText result =  (EditText) findViewById(R.id.text_edit_result);
        final RadioGroup rg =  (RadioGroup) findViewById(R.id.radio_group);
        final EventReader read = new EventReader(this, 's');

        Button button_return = (Button) findViewById(R.id.button_go_back_mod);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_return.setTypeface(typeface);

        button_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });


        Button button_silence = (Button) findViewById(R.id. button_silence_OK);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_silence.setTypeface(typeface);

        button_silence.setOnClickListener( new View.OnClickListener()
        {
            int rg_id;
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                String res = result.getText().toString();
                rg_id = rg.getCheckedRadioButtonId();
                read.get_note(name, res, 0, rg_id);
            }
        });
    }
}
