package com.midi.miditoscore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import static android.graphics.Bitmap.CompressFormat.PNG;

public class MusicObject
{
    public int value;
    public int velocity;
    public long tick;
    public long delta;
    public char type;

    public int x_pos;
    public int y_pos;

    public int width;
    public int height;

    public MusicObject(char ty, int val, int vel, long ti, long s)
    {
        value = val;
        type = ty;
        velocity = vel;
        tick = ti;
        delta = s;
        width = 21;
        height = 60;
    }

    public Bitmap convertToBitmap(Context c, int x, int y, int width, int height)
    {
        Drawable image = null;
        if(type == 'n')
            image = ContextCompat.getDrawable(c, R.drawable.eight_note);
        if(type == 'e')
            image = ContextCompat.getDrawable(c, R.drawable.eight_note);
        if(type == 'd')
            image = ContextCompat.getDrawable(c, R.drawable.eight_note_sharp);
        if(type == 'q')
            image = ContextCompat.getDrawable(c, R.drawable.quarter_note);
        if(type == 'a')
            image = ContextCompat.getDrawable(c, R.drawable.quarter_note_dot);
        if(type == 'c')
            image = ContextCompat.getDrawable(c, R.drawable.quarter_note_sharp);
        if(type == 'f')
            image = ContextCompat.getDrawable(c, R.drawable.quarter_note_dot_sharp);
        if(type == 'h')
            image = ContextCompat.getDrawable(c, R.drawable.half_note);
        if(type == 'b')
            image = ContextCompat.getDrawable(c, R.drawable.half_note_dot);
        if(type == 'x')
            image = ContextCompat.getDrawable(c, R.drawable.quarter_note_flat);
        if(type == 'z')
            image = ContextCompat.getDrawable(c, R.drawable.eight_note_flat);
        if(type == '!')
            image = ContextCompat.getDrawable(c, R.drawable.eight_note_sharp_do);
        if(type == '?')
            image = ContextCompat.getDrawable(c, R.drawable.sixth_note);
        if(type == '@')
            image = ContextCompat.getDrawable(c, R.drawable.trel);

        if(type == 'g')
            image = ContextCompat.getDrawable(c, R.drawable.down_eight_note);
        if(type == 'i')
            image = ContextCompat.getDrawable(c, R.drawable.down_quarter_note);
        if(type == 'o')
            image = ContextCompat.getDrawable(c, R.drawable.down_quarter_note_do);
        if(type == 'p')
            image = ContextCompat.getDrawable(c, R.drawable.down_quarter_note_mi);
        if(type == 'j')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note);
        if(type == 'k')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_do);
        if(type == 'l')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_re);
        if(type == 'm')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_mi);
        if(type == 'r')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_dot);
        if(type == 't')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_do_dot);
        if(type == 'u')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_mi_dot);
        if(type == 'v')
            image = ContextCompat.getDrawable(c, R.drawable.down_half_note_dot_sharp);

        if(type == 's')
            image = ContextCompat.getDrawable(c, R.drawable.staff);
        if(type == 'w')
            image = ContextCompat.getDrawable(c, R.drawable.line);

        Bitmap result_bitmap = drawableToBitmap(image, x, y, width, height);

        return result_bitmap;
    }


    public Bitmap drawableToBitmap(Drawable drawable, int x_pos, int y_pos, int w, int h)
    {
        Bitmap bitmap = null;

        bitmap = Bitmap.createBitmap(720, 1280, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(x_pos, y_pos, x_pos + w, y_pos + h);
        drawable.draw(canvas);

        return bitmap;
    }


}