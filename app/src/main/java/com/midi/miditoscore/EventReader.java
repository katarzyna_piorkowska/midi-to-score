package com.midi.miditoscore;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import com.leff.midi.MidiFile;
import com.leff.midi.MidiTrack;
import com.leff.midi.event.MidiEvent;
import com.leff.midi.event.NoteOn;
import com.leff.midi.event.meta.Tempo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.Vector;


public class EventReader extends ContextWrapper
{
    Vector <MusicObject> objects_treble;
    Vector <MusicObject> objects_bass;
    Vector <Tempo> vector_tempo;
    Vector <NoteOn> note_track_treble;
    Context c;

    char operation_type;

    public EventReader(Context base, char op)
    {
        super(base);
        c = base;
        operation_type = op;
    }

    public void get_note(String name, String res, double tempo_change, int rg_id)
    {
        File input = new File("storage/emulated/0/" + name);
        objects_treble = new Vector<>();
        objects_bass = new Vector<>();
        vector_tempo = new Vector<>();
        note_track_treble = new Vector<>();

        try
        {
            MidiFile midi = new MidiFile(input);
            MidiTrack track_treble = midi.getTracks().get(0);
            MidiTrack track_bass = midi.getTracks().get(1);

            Iterator<MidiEvent> it_treble = track_treble.getEvents().iterator();
            Iterator<MidiEvent> it_bass = track_bass.getEvents().iterator();
            int i = 0;

            while(it_treble.hasNext())
            {
                MidiEvent event = it_treble.next();
                if(event instanceof NoteOn)
                {
                    NoteOn note = (NoteOn) event;

                    objects_treble.add(new MusicObject('n', note.getNoteValue(), note.getVelocity(), note.getTick(), note.getDelta()));
                    i = i + 1;
                }
            }

            while(it_bass.hasNext())
            {
                MidiEvent event = it_bass.next();
                if(event instanceof NoteOn)
                {
                    NoteOn note = (NoteOn) event;

                    objects_bass.add(new MusicObject('n', note.getNoteValue(), note.getVelocity(), note.getTick(), note.getDelta()));
                    i = i + 1;
                }
            }

            ScoreCreator creator;
            if(operation_type == 'c')
                creator = new ScoreCreator(c, objects_treble, objects_bass);
            if(operation_type == 't')
            {

                Iterator<MidiEvent> it_modify = track_treble.getEvents().iterator();
                Iterator<MidiEvent> it_modified = track_treble.getEvents().iterator();

                Tempo tempoEvent = null;
                while(it_modify.hasNext())
                {
                    MidiEvent event = it_modify.next();
                    if(event instanceof Tempo)
                    {
                        tempoEvent = (Tempo)event;
                    }
                }

                float new_tempo = 0;
                if(tempo_change == 0.5)
                    new_tempo = tempoEvent.getBpm()/2;
                if(tempo_change == 0.75)
                    new_tempo = (tempoEvent.getBpm()*3)/4;
                if(tempo_change == 1.5)
                    new_tempo = (tempoEvent.getBpm()*3)/2;
                if(tempo_change == 2)
                    new_tempo = tempoEvent.getBpm()*2;

                while(it_modified.hasNext())
                {
                    MidiEvent event = it_modified.next();
                    if(event instanceof Tempo)
                    {
                        tempoEvent = (Tempo)event;
                        tempoEvent.setBpm(new_tempo);
                    }
                }
                File output = new File("storage/emulated/0/", res);
                try
                {
                    midi.writeToFile(output);
                }
                catch(IOException e)
                {
                    System.err.println(e);
                }
            }

            if(operation_type == 's')
            {
                if(rg_id == 2131165290)
                    midi.removeTrack(0);
                if(rg_id == 2131165291)
                    midi.removeTrack(1);

                File output = new File("storage/emulated/0/", res);
                try
                {
                    midi.writeToFile(output);
                }
                catch(IOException e)
                {
                    System.err.println(e);
                }
            }
        }
        catch (IOException e)
        {
            System.out.println("Error in loading midi file: " + e);
        }

    }

}
