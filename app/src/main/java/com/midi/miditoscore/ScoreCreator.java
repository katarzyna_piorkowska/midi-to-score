package com.midi.miditoscore;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import static android.graphics.Bitmap.CompressFormat.PNG;

public class ScoreCreator extends ContextWrapper
{
    Context c;
    Vector <MusicObject> notes_treble;
    Vector <MusicObject> breaks_treble;
    Vector <MusicObject> notes_bass;
    Vector <MusicObject> breaks_bass;

    int verses;
    int sharp_offset_treble;
    int sharp_offset_bass;
    boolean change_of_line;
    long length_of_line_in_ticks;
    ArrayList<Bitmap> list;

    public ScoreCreator(Context base, Vector t, Vector b)
    {
        super(base);
        c = base;
        notes_treble = t;
        notes_bass = b;
        breaks_treble = new Vector<>();
        breaks_bass = new Vector<>();
        verses = 0;
        sharp_offset_treble = 0;
        sharp_offset_bass = 0;
        change_of_line = true;

        create_graphic();
    }

    void create_graphic()
    {
        list = new ArrayList <Bitmap>();

        MusicObject staff = new MusicObject('s', 0, 0, 0, 0);
        Bitmap background = staff.convertToBitmap(c, 0, 0, 720, 1280);
        list.add(background);

        create_breaks_vector(notes_treble, breaks_treble);
        create_breaks_vector(notes_bass, breaks_bass);

        int treble_size, bass_size;
        if(notes_treble.size() == 144)
        {
            treble_size = 36;
            bass_size = 22;
        }
        else
        {
            treble_size = 56;
            bass_size = 47;
        }

        for(int i = 0; i < treble_size; i++)
        {
            check_type(i, notes_treble, breaks_treble, 't');
            int y_position = calculate_position_y(i, notes_treble, 't');
            int x_position = calculate_position_x(i, notes_treble, sharp_offset_treble, 't');
            if(notes_treble.size() != 144 && i == 38) {
                x_position = 76;
                y_position = 446;
            }

            Bitmap bitmap_with_music_object = notes_treble.get(i).convertToBitmap(c, x_position, y_position, notes_treble.get(i).width, notes_treble.get(i).height);
            list.add(bitmap_with_music_object);
        }

        verses = 0;
        change_of_line = true;
        int ticks_for_bar = 3 * 455;

        for(int i = 0; i < bass_size; i++)
        {
            check_type(i, notes_bass, breaks_bass, 'b');
            int x_position = calculate_position_x(i, notes_bass, sharp_offset_bass, 'b');
            int y_position = calculate_position_y(i, notes_bass, 'b');
            if(notes_treble.size() == 144 && i == 8)
                y_position = 180;

            Bitmap bitmap_with_music_object = notes_bass.get(i).convertToBitmap(c, x_position, y_position, notes_bass.get(i).width, notes_bass.get(i).height);
            list.add(bitmap_with_music_object);

        }


        long sum = 0;
        verses = 0;
        for(int i = 0; i < bass_size; i++)
        {
            long tick_difference = breaks_bass.get(i).tick - notes_bass.get(i).tick;

            if(i != 0) {
                if (notes_bass.get(i).x_pos < notes_bass.get(i - 1).x_pos) {
                    verses += 1;
                }
            }

            if(tick_difference + sum >= ticks_for_bar)
            {
                sum = 0;

                MusicObject line = new MusicObject('w', 0, 0, 0, 0);

                int x_position = notes_bass.get(i + 1).x_pos - 4;
                int y_position = 70 + verses * 206;
                if(notes_treble.size() == 144 && x_position == 99)
                    y_position = 276;

                Bitmap bitmap_with_music_object = line.convertToBitmap(c, x_position, y_position, 2, 136);
                list.add(bitmap_with_music_object);
            }
            else
                sum = sum + tick_difference;
        }

        Bitmap bitmapGroup = combineImageIntoOne(list);
        boolean isOKforGroup = saveBitmapToFile("Score.png", bitmapGroup, PNG, 100);
    }


    private Bitmap combineImageIntoOne(ArrayList<Bitmap> list)
    {
        int w = 0, h = 0;
        for (int i = 0; i < list.size(); i++) {
            if (i < list.size() - 1) {
                w = list.get(i).getWidth() > list.get(i + 1).getWidth() ? list.get(i).getWidth() : list.get(i + 1).getWidth();
            }
            h += list.get(i).getHeight();
        }

        Bitmap temp = Bitmap.createBitmap(720, 1280, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(temp);
        Rect RectForRender = new Rect(0,0,720, 1280);
        canvas.drawColor(Color.WHITE);
        for (int i = 0; i < list.size(); i++) {
            Log.d("HTML", "Combine: "+i+"/"+list.size()+1);
            canvas.drawBitmap(list.get(i), null, RectForRender, null );
        }
        return temp;
    }

    void check_type(int i, Vector <MusicObject> vec, Vector <MusicObject> breaks, char type)
    {
        long tick_difference = breaks.get(i).tick - vec.get(i).tick;
        if(type == 't')
        {
            if(tick_difference < 100)
                vec.get(i).type = '@';     // trel
            if (tick_difference == 113)
                vec.get(i).type = '?';     // sixth
            if (tick_difference == 227)
                vec.get(i).type = 'e';     // eight
            if (tick_difference == 340)
                vec.get(i).type = 'q';     // eight with dot
            if (tick_difference == 455)
                vec.get(i).type = 'q';     // quarter note
            if (tick_difference == 683)
                vec.get(i).type = 'a';     // quarter with dot
            if (tick_difference == 911)
                vec.get(i).type = 'h';     // half note
            if (tick_difference == 1367)
                vec.get(i).type = 'b';     // half note with dot
        }
        if(type == 'b')
        {
            if (tick_difference == 227)
                vec.get(i).type = 'g';     // eight down
            if (tick_difference == 455)
                vec.get(i).type = 'i';     // quarter note
            if (tick_difference == 911)
                vec.get(i).type = 'j';     // half note
            if (tick_difference == 1367)
                vec.get(i).type = 'r';     // half note with dot
        }
    }

    int calculate_position_x(int i, Vector <MusicObject> vec, int sharp_offset, char type)
    {

        int start_x_position = 50;
        int x = 0;

        double interval;
        if(notes_treble.size() == 144)
            interval = vec.get(i).tick/9;
        else
            interval = vec.get(i).tick/18;

        Integer interval_int = (int) (double) interval;


        if(i != 0)
        {
            int length_of_line_in_ticks_int = (int) (double) length_of_line_in_ticks;
            if(notes_treble.size() == 144)
                interval = (vec.get(i).tick - verses*length_of_line_in_ticks_int)/9;
            else
                interval = (vec.get(i).tick - verses*length_of_line_in_ticks_int)/18;
            interval_int = (int) (double) interval;
            if (vec.get(i - 1).width == 25)
            {
                x = start_x_position + interval_int + 10;
                sharp_offset++;
            }
            else
                x = start_x_position + interval_int + sharp_offset * 10;
        }
        else
            x = start_x_position + interval_int;

        if(notes_treble.size() == 144) {
            if (x > 630) {
                if (verses < 1)
                    verses += 1;

                if (type == 't' && change_of_line == true)
                    length_of_line_in_ticks = vec.get(i).tick;
                change_of_line = false;
            }
        }
        else {
            if (x > 660) {
                verses += 1;

                if (type == 't' && change_of_line == true)
                    length_of_line_in_ticks = vec.get(i).tick;
                change_of_line = false;
            }
        }

        vec.get(i).x_pos = x;
        return x;
    }

    int calculate_position_y(int i, Vector <MusicObject> vec, char type)
    {
        int y = 0;

        if(type == 't')
        {
            if (vec.get(i).value == 57)
                y = 86 + verses * 206;
            if (vec.get(i).value == 61)
            {
                y = 90 + verses * 206;
                if (vec.get(i).type == 'q')
                    vec.get(i).type = '!';
                if (vec.get(i).type == 'e')
                    vec.get(i).type = '!';
                if (vec.get(i).type == 'a')
                    vec.get(i).type = '!';
                vec.get(i).width = 25;
            }
            if (vec.get(i).value == 62)
                y = 88 + verses * 206;
            if (vec.get(i).value == 64)
                y = 83 + verses * 206;
            if (vec.get(i).value == 65)
                y = 78 + verses * 206;
            if (vec.get(i).value == 66)
                y = 78 + verses * 206;
            if (vec.get(i).value == 67)
                y = 72 + verses * 206;
            if (vec.get(i).value == 68) {
                y = 72 + verses * 206;
                if (vec.get(i).type == 'q')
                    vec.get(i).type = 'c';
                if (vec.get(i).type == 'e')
                    vec.get(i).type = 'd';
                if (vec.get(i).type == 'a')
                    vec.get(i).type = 'f';
                vec.get(i).width = 25;
            }
            if (vec.get(i).value == 69)
                y = 67 + verses * 206;
            if (vec.get(i).value == 70)
            {
                y = 61 + verses * 206;
                if (vec.get(i).type == 'q')
                    vec.get(i).type = 'x';
                if (vec.get(i).type == 'e')
                    vec.get(i).type = 'z';
                if (vec.get(i).type == 'a')
                    vec.get(i).type = 'z';
                vec.get(i).width = 25;
            }
            if (vec.get(i).value == 71)
                y = 61 + verses * 206;
            if (vec.get(i).value == 72)
                y = 56 + verses * 206;
            if (vec.get(i).value == 73)
            {
                y = 56 + verses * 206;
                if (vec.get(i).type == 'q')
                    vec.get(i).type = 'c';
                if (vec.get(i).type == 'e')
                    vec.get(i).type = 'd';
                if (vec.get(i).type == 'a')
                    vec.get(i).type = 'f';
                vec.get(i).width = 25;
            }
            if (vec.get(i).value == 74)
                y = 51 + verses * 206;
            if (vec.get(i).value == 76)
                y = 45 + verses * 206;
            if (vec.get(i).value == 77)
                y = 39 + verses * 206;
            if (vec.get(i).value == 79)
                y = 32 + verses * 206;
        }
        if(type == 'b')
        {
            if (vec.get(i).value == 38)
                y = 180 + verses * 206;
            if (vec.get(i).value == 41) {
                y = 175 + verses * 206;
                vec.get(i).type = 'q';
            }
            if (vec.get(i).value == 45)
                y = 169 + verses * 206;
            if (vec.get(i).value == 46)
                y = 164 + verses * 206;
            if (vec.get(i).value == 48)
                y = 158 + verses * 206;
            if (vec.get(i).value == 49)
                y = 158 + verses * 206;
            if (vec.get(i).value == 50)
                y = 152 + verses * 206;
            if (vec.get(i).value == 52)
                y = 147 + verses * 206;
            if (vec.get(i).value == 53)
                y = 142 + verses * 206;
            if (vec.get(i).value == 55)
                y = 136 + verses * 206;
            if (vec.get(i).value == 56)
            {
                y = 136 + verses * 206;
                if(vec.get(i).type == 'j')
                    vec.get(i).type = 'v';
                if(vec.get(i).type == 'i')
                    vec.get(i).type = 'v';
                if(vec.get(i).type == 'r')
                    vec.get(i).type = 'v';
            }
            if (vec.get(i).value == 57)
                y = 130 + verses * 206;
            if (vec.get(i).value == 59)
                y = 125 + verses * 206;
            if (vec.get(i).value == 60)
            {
                y = 120 + verses * 206;
                if(vec.get(i).type == 'j')
                    vec.get(i).type = 'k';
                if(vec.get(i).type == 'i')
                    vec.get(i).type = 'o';
                if(vec.get(i).type == 'r')
                    vec.get(i).type = 't';
            }
            if (vec.get(i).value == 62)
            {
                y = 115 + verses * 206;
                vec.get(i).type = 'l';
            }
            if (vec.get(i).value == 64)
            {
                y = 110 + verses * 206;
                if(vec.get(i).type == 'j')
                    vec.get(i).type = 'm';
                if(vec.get(i).type == 'i')
                    vec.get(i).type = 'p';
                if(vec.get(i).type == 'r')
                    vec.get(i).type = 'u';
            }
        }
        return y;
    }

    void create_breaks_vector(Vector <MusicObject> vec, Vector <MusicObject> breaks)
    {
        for(int i = 0; i < vec.size(); i++)
            if(vec.get(i).velocity == 0)
            {
                breaks.add(vec.get(i));
                vec.remove(i);
                i = i - 1;
            }
    }

    boolean saveBitmapToFile(String fileName, Bitmap bm, Bitmap.CompressFormat format, int quality)
    {
        File imageFile = new File("storage/emulated/0/", fileName);

        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(imageFile);
            bm.compress(format,quality,fos);
            fos.close();
            return true;
        }
        catch (IOException e)
        {
            Log.e("app",e.getMessage());
            if (fos != null)
            {
                try
                {
                    fos.close();
                } catch (IOException e1)
                {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }
}