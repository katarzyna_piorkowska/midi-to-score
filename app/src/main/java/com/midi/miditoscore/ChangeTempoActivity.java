package com.midi.miditoscore;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChangeTempoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_tempo);

        final EditText music_name =  (EditText) findViewById(R.id.text_edit_mod);
        final EditText result =  (EditText) findViewById(R.id.text_edit_result);
        final EventReader read = new EventReader(this, 't');

        Button button_return = (Button) findViewById(R.id.button_go_back_mod);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_return.setTypeface(typeface);

        button_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });



        Button button_change_tempo_05 = (Button) findViewById(R.id.button_tempo_05);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_change_tempo_05.setTypeface(typeface);

        button_change_tempo_05.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                String res = result.getText().toString();
                read.get_note(name, res, 0.5, 0);
            }
        });

        Button button_change_tempo_75 = (Button) findViewById(R.id.button_tempo_75);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_change_tempo_75.setTypeface(typeface);

        button_change_tempo_75.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                String res = result.getText().toString();
                read.get_note(name, res, 0.75, 0);
            }
        });

        Button button_change_tempo_15 = (Button) findViewById(R.id.button_tempo_15);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_change_tempo_15.setTypeface(typeface);

        button_change_tempo_15.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                String res = result.getText().toString();
                read.get_note(name, res, 1.5, 0);
            }
        });

        Button button_change_tempo_2 = (Button) findViewById(R.id.button_tempo_2);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/cafe.ttf");
        button_change_tempo_2.setTypeface(typeface);

        button_change_tempo_2.setOnClickListener( new View.OnClickListener()
        {
            public void onClick (View v)
            {
                String name = music_name.getText().toString();
                String res = result.getText().toString();
                read.get_note(name, res, 2, 0);
            }
        });
    }
}
